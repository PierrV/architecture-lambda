# Architecture Lambda

Kafka + Spark + Elastic


1 - Start Zookeeper and Kafka
2 - Start Elasticsearch and Kibana
    ./<repo_elasticsearch>/bin/elastic
    ./<repo_kibana>/bin/kibana
3 - Start Spark.scala
4 - Put the .csv here => "C:\gitroot\GIT\architecture-lambda\DroneKafka\src\ParkingViolation.csv"
5 - Start Producer.scala