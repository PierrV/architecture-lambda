name := "Spark - ELK"

version := "0.1"

scalaVersion := "2.12.1"

libraryDependencies += "org.elasticsearch" % "elasticsearch-hadoop" % "7.3.0"
libraryDependencies += "org.apache.kafka" % "kafka-clients" % "2.3.0"
libraryDependencies += "org.apache.spark" %% "spark-streaming" % "2.4.4"
libraryDependencies += "org.apache.spark" %% "spark-streaming-kafka-0-10" % "2.4.4"
libraryDependencies += "org.slf4j" % "slf4j-api" % "1.8.0-beta4"
libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.26" % Test
libraryDependencies += "org.slf4j" % "slf4j-log4j12" % "1.7.25" % Test
