package Lambda

import java.util.Properties
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

object Producer {
  def main(args: Array[String]): Unit = {

    val props: Properties = new Properties()
    props.put("bootstrap.servers", "localhost:9092")
    props.put("key.serializer",
      "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer",
      "org.apache.kafka.common.serialization.StringSerializer")
    props.put("acks", "all")
    val producer = new KafkaProducer[String, String](props)
    val topic = "text_topic"
    try {
      for (i <- 0 to 15) {

        val bufferedSource = io.Source.fromFile("C:\\gitroot\\GIT\\architecture-lambda\\DroneKafka\\src\\ParkingViolation.csv")
        for (line <- bufferedSource.getLines) {

          val record = new ProducerRecord[String, String](topic, i.toString, line)
          val metadata = producer.send(record)

        }
        bufferedSource.close

      }
    } catch {
      case e: Exception => e.printStackTrace()
    } finally {
      producer.close()
    }
  }
}